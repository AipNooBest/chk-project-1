import os

digit_1 = os.environ.get("DIGIT_1")
digit_2 = os.environ.get("DIGIT_2")

if len(digit_1) > len(digit_2):
    print(digit_1)
else:
    print(digit_2)
